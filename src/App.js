import React, { Component } from 'react';
import { Provider } from 'react-redux';
import {View} from 'react-native';
import { createStore,applyMiddleware } from 'redux';
import firebase from 'firebase'; 
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';
import LoginForm from './components/LoginForm';
import Router from './Router';

class App extends Component {
    componentWillMount(){
      const config =  {
            apiKey: 'AIzaSyCjXpvgGXIasic36c8-j5HrnYT6L9oBnxI',
            authDomain: 'manager-66eff.firebaseapp.com',
            databaseURL: 'https://manager-66eff.firebaseio.com',
            projectId: 'manager-66eff',
            storageBucket: 'manager-66eff.appspot.com',
            messagingSenderId: '635106292398'
          };

          firebase.initializeApp(config);
    }
    render() {
        const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
        return(
            <Provider store={store}>
                <View>
                    //TODO FIX THAT
                    <Router/>
                </View>
            </Provider>
        );
    }
}

export default App;